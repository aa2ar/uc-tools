/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   appaboutdialog.h
* \author Alex Arefyev
* \date   08.02.2018
*
*
*/

#ifndef __APPABOUT_DIALOG__INCLUDED__
#define __APPABOUT_DIALOG__INCLUDED__

#include <QDialog>

namespace Ui {
class AboutDialog;
}

class AppAboutDialog : public QDialog
{
    Q_OBJECT

public:
    AppAboutDialog(QWidget * parent = 0);

private:
    Ui::AboutDialog *ui;
};

#endif // __APPABOUT_DIALOG__INCLUDED__
