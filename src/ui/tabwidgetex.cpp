/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   tabwidgetex.cpp
* \author Alex Arefyev
* \date   08.02.2018
* 
* 
*/

#include "tabwidgetex.h"
#include <QToolButton>
#include <QTabBar>
#include <QSize>

TabWidgetEx::TabWidgetEx(QWidget *parent)
    : QTabWidget(parent)
{
}

void TabWidgetEx::setSpacing(int value)
{
    mSpacing = value;
    updateLayout({0, 0, width(), height()});
}

void TabWidgetEx::addWidget(QWidget * w)
{
    mWidgets.append(w);
    w->setParent(this);
    w->setFixedSize(w->sizeHint());
}

void TabWidgetEx::addButton(QToolButton * btn)
{
    mWidgets.append(btn);
    btn->setParent(this);
    btn->setIconSize(tabBar()->iconSize());
    btn->setFixedSize(btn->sizeHint());
}

void TabWidgetEx::updateLayout()
{
    updateLayout({0, 0, width(), height()});
}

void TabWidgetEx::resizeEvent(QResizeEvent * event)
{
    QTabWidget::resizeEvent(event);
    updateLayout({0, 0, width(), height()});
}

void TabWidgetEx::updateLayout(const QRect & r)
{
    int x = r.width();
    foreach (QWidget * w, mWidgets) {
        QSize sz = w->minimumSize();
        if (sz.isEmpty())
            sz = w->sizeHint();
        x -= sz.width();
        w->setGeometry(x, 0, sz.width(), sz.height());
        x -= mSpacing;
    }
}

