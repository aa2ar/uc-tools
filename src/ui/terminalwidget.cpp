/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   terminalwidget.cpp
* \author Alex Arefyev
* \date   08.02.2018
*
*
*/
#include "terminalwidget.h"
#include "ui_terminalwidget.h"
#include "../core/appmanager.h"
#include "../core/appsettings.h"
#include "../core/appsettingsdata.h"
#include "../core/appstatedata.h"
#include <QLineEdit>
#include <QFileInfo>
#include <QSerialPort>
#include <QLabel>
#include <QToolButton>
#include <QDebug>

static
void appendText(QPlainTextEdit * edit, QString s)
{
    QTextCursor prev_cursor = edit->textCursor();
    edit->moveCursor(QTextCursor::End);
    edit->insertPlainText(s);
    edit->setTextCursor(prev_cursor);
}

TerminalWidget::TerminalWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TerminalWidget)
{
    ui->setupUi(this);
    ui->cbxTerminalInput->lineEdit()->setClearButtonEnabled(true);
    //ui->cbxTerminalInput->insertItem(0, QString());
    //ui->cbxTerminalInput->setCurrentIndex(0);

    connect(ui->cbxTerminalInput->lineEdit(), SIGNAL(returnPressed()),
            this, SLOT(onTerminalInputReturnPressed()));
}

TerminalWidget::~TerminalWidget()
{
    delete ui;
}

void TerminalWidget::setManager(AppManager * mgr)
{
    if (mManager) {
        disconnect(mManager, SIGNAL(event(unsigned)), this, SLOT(managerEvent(unsigned)));
        disconnect(mManager, SIGNAL(terminalOutput(QVector<QByteArray>)),
                   this,     SLOT  (terminalOutput(QVector<QByteArray>)));
    }
    mManager = mgr;
    if (mManager) {
        connect(mManager, SIGNAL(event(unsigned)), this, SLOT(managerEvent(unsigned)));
        connect(mManager, SIGNAL(terminalOutput(QVector<QByteArray>)),
                this,     SLOT  (terminalOutput(QVector<QByteArray>)));
        managerEvent(RestartTerminal|StateChanged);
    }
}

QStringList TerminalWidget::inputHistory() const
{
    QStringList list;

    for (int i = 0; i < ui->cbxTerminalInput->count(); ++i) {
        QString item = ui->cbxTerminalInput->itemText(i);
        if (!item.isEmpty())
            list.append(item);
    }

    return list;
}

void TerminalWidget::setInputHistory(QStringList list)
{
    ui->cbxTerminalInput->clear();
    ui->cbxTerminalInput->addItems(list);
    ui->cbxTerminalInput->lineEdit()->clear();
}

void TerminalWidget::managerEvent(unsigned events)
{
    if (events & RestartTerminal)
        ui->txtTerminalOutput->clear();
    if (events & StateChanged)
        enableWidgets(mManager->state() & TerminalOK);
}

void TerminalWidget::terminalOutput(const QVector<QByteArray> & items)
{
    appendText(ui->txtTerminalOutput, items[0]);
}

void TerminalWidget::onTerminalInputReturnPressed()
{
    if (mManager) {
        QString s = ui->cbxTerminalInput->currentText();
        if (!s.isEmpty()) {
            mManager->terminalInput(s);
            if (mManager->settings()->data()->terminal.clearInputAfterSent)
                ui->cbxTerminalInput->lineEdit()->clear();
            if (ui->cbxTerminalInput->findText(s) == -1)
                ui->cbxTerminalInput->addItem(s);
        }
    }
}

void TerminalWidget::on_btnRestart_clicked()
{
    if (mManager)
        mManager->restartTerminal();
}

void TerminalWidget::on_btnClear_clicked()
{
    ui->txtTerminalOutput->clear();
}

void TerminalWidget::on_cbxTerminalInput_editTextChanged(const QString & text)
{
    ui->btnTerminalSend->setEnabled(!text.isEmpty());
}

void TerminalWidget::on_btnTerminalSend_clicked()
{
    onTerminalInputReturnPressed();
}

void TerminalWidget::enableWidgets(bool enable)
{
    ui->txtTerminalOutput->setEnabled(enable);
    ui->cbxTerminalInput->setEnabled(enable);
    ui->btnClear->setEnabled(enable);
    ui->btnTerminalSend->setEnabled(enable
                                    ? !ui->cbxTerminalInput->lineEdit()->text().isEmpty()
                                    : false);
}
