/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   tabwidgetex.h
* \author Alex Arefyev
* \date   08.02.2018
* 
* 
*/

#ifndef __TABWIDGETEX__INCLUDED__
#define __TABWIDGETEX__INCLUDED__

#include <QTabWidget>

class QToolButton;
class TabWidgetEx : public QTabWidget
{
    Q_OBJECT

public:
    explicit TabWidgetEx(QWidget * parent = 0);

    void setSpacing(int);
    void addWidget(QWidget *);
    void addButton(QToolButton *);

    void updateLayout();

protected:
    void resizeEvent(QResizeEvent *) Q_DECL_OVERRIDE;

private:
    int mSpacing = 0;
    QList<QWidget *> mWidgets;

    void updateLayout(const QRect &);
};

#endif /* __TABWIDGETEX__INCLUDED__ */
