/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   collapsiblewidgetcontainer.h
* \author Alex Arefyev
* \date   08.02.2018
*
*
*/

#ifndef __COLLAPSIBLE_WIDGET_CONTAINER__INCLUDED__
#define __COLLAPSIBLE_WIDGET_CONTAINER__INCLUDED__

#include <QSplitter>
#include "collapsiblewidget.h"

class CollapsibleWidget;
class CollapsibleWidgetContainer : public QSplitter, public CollapsibleWidgetManager
{
public:
    explicit CollapsibleWidgetContainer(QWidget *parent = 0);
};

#endif // __COLLAPSIBLE_WIDGET_CONTAINER__INCLUDED__
