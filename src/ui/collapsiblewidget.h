/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   collapsiblewidget.h
* \author Alex Arefyev
* \date   08.02.2018
*
*
*/

#ifndef __COLLAPSIBLE_WIDGET__INCLUDED__
#define __COLLAPSIBLE_WIDGET__INCLUDED__

#include <QFrame>
#include <QIcon>

class CollapsibleWidget;
class CollapsibleWidgetManager
{
public:
    CollapsibleWidgetManager();
    virtual ~CollapsibleWidgetManager();

    void addCWidget(CollapsibleWidget *);
    void removeCWidget(CollapsibleWidget *);

    bool canCollapse(CollapsibleWidget *);
    void collapsed(CollapsibleWidget *);
    void hid(CollapsibleWidget *);

private:
    class Private;
    Private * p;

    bool contains(CollapsibleWidget *);
};

class QStyleOptionFrame;
class QEvent;
class QPaintEvent;
class QMouseEvent;
class QResizeEvent;
class CollapsibleWidget : public QFrame
{
    Q_OBJECT
public:
    explicit CollapsibleWidget(QWidget *parent = 0);
    ~CollapsibleWidget();

    void setManager(CollapsibleWidgetManager *);

    void collapse();
    void expand();

    bool isCollapsed() const;

    void setIconSize(QSize);
    void setIcons(QIcon i1, QIcon i2);
    void setWidget(QWidget *);

    bool isWidgetEnabled() const;
    void setWidgetEnabled(bool);

signals:
    void collapsed();
    void hid();

public slots:

protected:
    bool event(QEvent *) Q_DECL_OVERRIDE;
    void paintEvent(QPaintEvent *) Q_DECL_OVERRIDE;
    void resizeEvent(QResizeEvent *) Q_DECL_OVERRIDE;
    void mousePressEvent(QMouseEvent *) Q_DECL_OVERRIDE;
    void hideEvent(QHideEvent *) Q_DECL_OVERRIDE;
    void changeEvent(QEvent *) Q_DECL_OVERRIDE;

private:
    class Private;
    Private * p;

    void drawFrame(QPainter *);
    void drawHeader(QPainter *);
};

#endif // __COLLAPSIBLE_WIDGET__INCLUDED__
