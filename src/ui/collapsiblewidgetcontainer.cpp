/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   collapsiblewidgetcontainer.cpp
* \author Alex Arefyev
* \date   08.02.2018
*
*
*/

#include "collapsiblewidgetcontainer.h"

// CollapsibleWidgetContainer
CollapsibleWidgetContainer::CollapsibleWidgetContainer(QWidget *parent)
    : QSplitter(parent)
{}

