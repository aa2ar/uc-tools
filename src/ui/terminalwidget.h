/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   terminalwidget.h
* \author Alex Arefyev
* \date   08.02.2018
*
*
*/

#ifndef __TERMINALWIDGET__INCLUDED__
#define __TERMINALWIDGET__INCLUDED__

#include <QWidget>

namespace Ui {
class TerminalWidget;
}

class QLabel;
class AppManager;
class TerminalWidget : public QWidget
{
    Q_OBJECT

public:
    enum {
        TerminalTab = 0,
    };

public:
    explicit TerminalWidget(QWidget *parent = 0);
    ~TerminalWidget();

    void setManager(AppManager *);

    QStringList inputHistory() const;
    void setInputHistory(QStringList);

signals:
    void settingsRequest();

private slots:
    void managerEvent(unsigned);
    void terminalOutput(const QVector<QByteArray> &);
    void onTerminalInputReturnPressed();

    void on_btnRestart_clicked();
    void on_btnClear_clicked();
    void on_cbxTerminalInput_editTextChanged(const QString &);
    void on_btnTerminalSend_clicked();

private:
    Ui::TerminalWidget *ui;
    AppManager * mManager = nullptr;

    void enableWidgets(bool);
};

#endif // __TERMINALWIDGET__INCLUDED__
