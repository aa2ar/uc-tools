/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   debuggerwidget.h
* \author Alex Arefyev
* \date   08.02.2018
*
*
*/

#ifndef __DEBUGGER_WIDGET__INCLUDED__
#define __DEBUGGER_WIDGET__INCLUDED__

#include <QFrame>

namespace Ui {
class DebuggerWidget;
}

class AppManager;
class DebuggerWidget : public QFrame
{
    Q_OBJECT

public:
    enum {
        DebuggerTab = 0,
        TelnetTab = 1,
    };

public:
    explicit DebuggerWidget(QWidget *parent = 0);
    ~DebuggerWidget();

    void setManager(AppManager *);
    void showTab(int);

signals:
    void telnetInput(QString);

private slots:
    void managerEvent(unsigned);
    void debuggerOutput(QString);
    void telnetOutput(QString);
    void btnRestartClicked();
    void onTelnetInputReturnPressed();

    void on_tabWidget_currentChanged(int index);
    void on_cbxTelnetInput_editTextChanged(const QString &arg1);
    void on_btnTelnetSend_clicked();

private:
    Ui::DebuggerWidget *ui;
    AppManager * mManager = nullptr;
    QString mScriptPath;
};

#endif // __DEBUGGER_WIDGET__INCLUDED__
