/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   mainwindow.h
* \author Alex Arefyev
* \date   08.02.2018
*
*
*/

#ifndef __MAINWINDOW__INCLUDED__
#define __MAINWINDOW__INCLUDED__

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class QShortcut;
class TerminalHistory;
class AppManager;
class DebuggerWidget;
class TerminalWidget;
class CollapsibleWidgetManager;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(AppManager *, TerminalHistory *, QWidget * parent = 0);
    ~MainWindow();

protected:
    void showEvent(QShowEvent *) Q_DECL_OVERRIDE;

private slots:
    void managerEvent(unsigned);
    void on_btnSettings_clicked();
    void on_btnInfo_clicked();

private:
    Ui::MainWindow *ui;
    AppManager * mManager = nullptr;
    TerminalHistory * mTerminalHistory = nullptr;
    DebuggerWidget * mDebuggerWidget = nullptr;
    TerminalWidget * mTerminalWidget = nullptr;
    QShortcut * mSctInfo = nullptr;
    QShortcut * mSctSettings = nullptr;
    QShortcut * mSctShowDebuggerTab = nullptr;
    QShortcut * mSctShowTelnetTab = nullptr;
    int mCurrentSettingsTab = 0;
    CollapsibleWidgetManager * mWinManager = nullptr;

    void setupShortcuts();
    void changeWindowOrientation();
};

#endif // __MAINWINDOW__INCLUDED__
