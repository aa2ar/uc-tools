/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   debuggerwidget.cpp
* \author Alex Arefyev
* \date   08.02.2018
*
*
*/

#include "debuggerwidget.h"
#include "ui_debuggerwidget.h"
#include "../core/appmanager.h"
#include "../core/appsettings.h"
#include "../core/appsettingsdata.h"
#include "../core/appstatedata.h"
#include <QLineEdit>
#include <QToolButton>
#include <QDebug>

static
const char * tooltipTemplate =
"<table><tr><td><b>%1</b></td></tr><tr><td style='white-space:pre'>%2</td></tr></table>";

static
QString buildDebuggerTabTooltip(const AppSettingsData * data)
{
    return QString(tooltipTemplate)
            .arg(data->debugger.command)
            .arg(data->debugger.parameters);
}

static
QString buildTelnetTabTooltip(const AppSettingsData * data)
{
    return QString(tooltipTemplate)
            .arg(data->debugger.telnet.command)
            .arg(data->debugger.telnet.parameters);
}

static
void appendText(QPlainTextEdit * edit, QString s)
{
    QTextCursor prev_cursor = edit->textCursor();
    edit->moveCursor(QTextCursor::End);
    edit->insertPlainText(s);
    edit->setTextCursor(prev_cursor);
}

DebuggerWidget::DebuggerWidget(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::DebuggerWidget)
{
    ui->setupUi(this);
    ui->cbxTelnetInput->lineEdit()->setClearButtonEnabled(true);
    ui->cbxTelnetInput->addItems(AppManager::telnetCommands());
    ui->cbxTelnetInput->insertItem(0, QString());
    ui->cbxTelnetInput->setCurrentIndex(0);

    QSize iconSize = ui->tabWidget->tabBar()->iconSize();

    QToolButton * btn = new QToolButton();
    btn->setIconSize(iconSize);
    btn->setIcon(QIcon(":/rc/img/reload-24x24.png"));
    btn->setToolTip(tr("Restart debugger"));
    btn->setFixedSize(28, 28);
    ui->tabWidget->addWidget(btn);
    connect(btn, SIGNAL(clicked()), this, SLOT(btnRestartClicked()));


    connect(ui->cbxTelnetInput->lineEdit(), SIGNAL(returnPressed()),
            this, SLOT(onTelnetInputReturnPressed()));

    //
    setMouseTracking(true);
}

DebuggerWidget::~DebuggerWidget()
{
    delete ui;
}

void DebuggerWidget::setManager(AppManager * mgr)
{
    if (mManager) {
        disconnect(mManager, SIGNAL(event(unsigned)), this, SLOT(managerEvent(unsigned)));
        disconnect(mManager, SIGNAL(debuggerOutput(QString)), this, SLOT(debuggerOutput(QString)));
        disconnect(mManager, SIGNAL(telnetOutput(QString)), this, SLOT(telnetOutput(QString)));
    }
    mManager = mgr;
    if (mManager) {
        connect(mManager, SIGNAL(event(unsigned)), this, SLOT(managerEvent(unsigned)));
        connect(mManager, SIGNAL(debuggerOutput(QString)), this, SLOT(debuggerOutput(QString)));
        connect(mManager, SIGNAL(telnetOutput(QString)), this, SLOT(telnetOutput(QString)));
        managerEvent(RestartTelnet|RestartDebugger);
    }
}

void DebuggerWidget::showTab(int tabIdx)
{
    if (ui->tabWidget->currentIndex() == tabIdx) {
        if (tabIdx == TelnetTab)
            ui->cbxTelnetInput->setFocus();
    }
    else
        ui->tabWidget->setCurrentIndex(tabIdx);
}

void DebuggerWidget::managerEvent(unsigned events)
{
    if (events & RestartTelnet)
        ui->txtTelnetOutput->clear();
    else if (events & RestartDebugger) {
        ui->debuggerOutput->clear();
        ui->txtTelnetOutput->clear();
    }
    if (events & StateChanged) {
        ui->tabWidget->setTabToolTip(DebuggerTab,
                                     buildDebuggerTabTooltip(mManager->settings()->data()));
        ui->tabWidget->setTabToolTip(TelnetTab,
                                     buildTelnetTabTooltip(mManager->settings()->data()));

        ui->tabWidget->widget(DebuggerTab)->setEnabled(mManager->state() & DebuggerOK);
        ui->tabWidget->widget(TelnetTab)->setEnabled(mManager->state() & TelnetOK);
    }
}

void DebuggerWidget::debuggerOutput(QString s)
{
    appendText(ui->debuggerOutput, s);
}

void DebuggerWidget::telnetOutput(QString s)
{
    appendText(ui->txtTelnetOutput, s);
}

void DebuggerWidget::btnRestartClicked()
{
    mManager->restartDebugger();
}

void DebuggerWidget::onTelnetInputReturnPressed()
{
    if (mManager) {
        QString s = ui->cbxTelnetInput->currentText();
        if (!s.isEmpty()) {
            ui->cbxTelnetInput->lineEdit()->clear();
            mManager->telnetInput(s);
        }
    }
}

void DebuggerWidget::on_tabWidget_currentChanged(int index)
{
    if (index == TelnetTab)
        ui->cbxTelnetInput->setFocus();
}

void DebuggerWidget::on_cbxTelnetInput_editTextChanged(const QString & text)
{
    ui->btnTelnetSend->setEnabled(!text.isEmpty());
}

void DebuggerWidget::on_btnTelnetSend_clicked()
{
    onTelnetInputReturnPressed();
}
