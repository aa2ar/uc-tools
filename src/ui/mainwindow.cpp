/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   mainwindow.cpp
* \author Alex Arefyev
* \date   08.02.2018
*
*
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "../core/appmanager.h"
#include "../core/appsettings.h"
#include "../core/appsettingsdata.h"
#include "../core/appstatedata.h"
#include "../core/terminalhistory.h"
#include "debuggerwidget.h"
#include "terminalwidget.h"
#include "appsettingsdialog.h"
#include "appaboutdialog.h"
#include "collapsiblewidget.h"
#include <QResizeEvent>
#include <QIcon>
#include <QShortcut>
#include <QDebug>


static
const char * tooltipTemplate =
"<table><tr><td style='white-space:pre'>%1</td></tr></table>";

QString buildInfoText(const AppSettingsData * data)
{
    QString parity
            = data->terminal.parity == 0
            ? QStringLiteral("N")
            : (data->terminal.parity == 1 ? QStringLiteral("O") : QStringLiteral("E"));
    return QString("%1, %2, %3%4%5")
            .arg(data->terminal.port)
            .arg(data->terminal.baudrate)
            .arg(data->terminal.databits)
            .arg(parity)
            .arg(data->terminal.stopbits);
}

MainWindow::MainWindow(AppManager * mgr, TerminalHistory * hist, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    mTerminalHistory(hist),
    mManager(mgr)
{
    ui->setupUi(this);
    mWinManager = new CollapsibleWidgetManager();
//    mWinManager->addWidget(ui->debuggerContainer);
//    mWinManager->addWidget(ui->terminalContainer);

    mDebuggerWidget = new DebuggerWidget(ui->splitter);
    mDebuggerWidget->setManager(mManager);
    mTerminalWidget = new TerminalWidget(ui->splitter);
    mTerminalWidget->setManager(mManager);

    QIcon i1(QStringLiteral(":/rc/img/toggle_plus-20x20.png"));
    QIcon i2(QStringLiteral(":/rc/img/toggle_minus-20x20.png"));

//    ui->debuggerContainer->setManager(mWinManager);
    ui->debuggerContainer->setWindowTitle(tr("Debugger"));
    ui->debuggerContainer->setIcons(i1, i2);
    ui->debuggerContainer->setWidget(mDebuggerWidget);

//    ui->terminalContainer->setManager(mWinManager);
    ui->terminalContainer->setWindowTitle(tr("Terminal"));
    ui->terminalContainer->setIcons(i1, i2);
    ui->terminalContainer->setWidget(mTerminalWidget);

    connect(mManager, SIGNAL(event(uint)), this, SLOT(managerEvent(uint)));

    mTerminalWidget->setInputHistory(mTerminalHistory->get());

    setupShortcuts();
}

MainWindow::~MainWindow()
{
    QStringList hist = mTerminalWidget->inputHistory();
    if (!hist.isEmpty())
        mTerminalHistory->set(hist);

    delete ui;
}

void MainWindow::showEvent(QShowEvent *)
{
    mManager->start();
}

void MainWindow::managerEvent(unsigned events)
{
    if (events & StateChanged) {
        ui->debuggerContainer->setVisible(mManager->settings()->data()->debugger.use);
        ui->terminalContainer->setVisible(mManager->settings()->data()->terminal.useTerminal);

        if (mManager->state() & TerminalOK)
            ui->terminalContainer->setToolTip(buildInfoText(mManager->settings()->data()));
    }
}

void MainWindow::setupShortcuts()
{
    mSctInfo = new QShortcut(this);
    mSctInfo->setKey(QKeySequence(Qt::Key_F1));
    connect(mSctInfo, SIGNAL(activated()), this, SLOT(on_btnInfo_clicked()));

    mSctSettings = new QShortcut(this);
    mSctSettings->setKey(QKeySequence(Qt::Key_F2));
    connect(mSctSettings, SIGNAL(activated()), this, SLOT(on_btnSettings_clicked()));

    mSctShowDebuggerTab = new QShortcut(this);
    mSctShowDebuggerTab->setKey(QKeySequence("Ctrl+1"));
    connect(mSctShowDebuggerTab, &QShortcut::activated, this, [this] {
        mDebuggerWidget->showTab(DebuggerWidget::DebuggerTab);
    });

    mSctShowTelnetTab = new QShortcut(this);
    mSctShowTelnetTab->setKey(QKeySequence("Ctrl+2"));
    connect(mSctShowTelnetTab, &QShortcut::activated, this, [this] {
        mDebuggerWidget->showTab(DebuggerWidget::TelnetTab);
    });
}

void MainWindow::changeWindowOrientation()
{
    QRect geom = geometry();
    Qt::Orientation orient = ui->splitter->orientation();
    if (orient == Qt::Vertical) {
        orient = Qt::Horizontal;
    }
    else {
        orient = Qt::Vertical;
    }
    QRect newGeom = {geom.x(), geom.y(), geom.height(), geom.width()};
    setGeometry(newGeom);
    ui->splitter->setOrientation(orient);
}

void MainWindow::on_btnSettings_clicked()
{
    AppSettingsDialog dlg(mManager->settings(), mCurrentSettingsTab);
    if (dlg.exec() == QDialog::Accepted)
        mManager->setSettings(AppSettingsData::Debugger|AppSettingsData::Terminal, dlg.data());

    mCurrentSettingsTab = dlg.currentTab();
}

void MainWindow::on_btnInfo_clicked()
{
    AppAboutDialog().exec();
}
