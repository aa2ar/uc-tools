/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   appsettingsdialog.cpp
* \author Alex Arefyev
* \date   08.02.2018
*
*
*/

#include "appsettingsdialog.h"
#include "ui_appsettingsdialog.h"
#include "../core/appsettings.h"
#include "../core/appsettingsdata.h"
#include "../core/appmanager.h"
#include <QFileDialog>
#include <QFileInfo>
#include <QLineEdit>
#include <QPushButton>
#include <QGroupBox>
#include <QDir>
#include <QDebug>

template<typename T>
bool isModified(const T & t1, const T & t2)
{
    return (t1 != t2);
}

int fillComboBox(QComboBox * cbx, const QList<QPair<QString, int> > & list, int defVal = 0)
{
    cbx->clear();

    int i = 0, current = -1;
    for (auto p: list) {
        cbx->addItem(p.first, p.second);
        if (p.second == defVal)
            current = i;

        ++i;
    }

    cbx->setCurrentIndex(current);

    return current;
}

AppSettingsDialog::AppSettingsDialog(const AppSettings * as, int tabIdx, QWidget * parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog),
    mSettingsData(new AppSettingsData)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog);

    connect(this, SIGNAL(accepted()), this, SLOT(applySettings()));
    // debugger
    connect(ui->grpDebugger, SIGNAL(clicked(bool)), this, SLOT(changed(bool)));
    connect(ui->edtDebuggerCommand, SIGNAL(textChanged(QString)), this, SLOT(changed(QString)));
    connect(ui->edtDebuggerParams, SIGNAL(textChanged(QString)), this, SLOT(changed(QString)));
    connect(ui->grpTelnet, SIGNAL(clicked(bool)), this, SLOT(changed(bool)));
    // terminal
    connect(ui->edtTerminalPort, SIGNAL(textChanged(QString)), this, SLOT(changed(QString)));
    connect(ui->cbxProtocol, SIGNAL(currentIndexChanged(int)), this, SLOT(changed(int)));
    connect(ui->cbxClearInputAfterSent, SIGNAL(clicked(bool)), this, SLOT(changed(bool)));
    connect(ui->cbxTerminalBaudrate, SIGNAL(currentIndexChanged(int)), this, SLOT(changed(int)));
    connect(ui->cbxTerminalDataBits, SIGNAL(currentIndexChanged(int)), this, SLOT(changed(int)));
    connect(ui->cbxTerminalParity, SIGNAL(currentIndexChanged(int)), this, SLOT(changed(int)));
    connect(ui->cbxTerminalStopBits, SIGNAL(currentIndexChanged(int)), this, SLOT(changed(int)));

    as->copySettings(mSettingsData);
    initialize(mSettingsData);

    int tab = tabIdx;
    if (tab >= 0 && tab < ui->tabWidget->count())
        ui->tabWidget->setCurrentIndex(tab);

    //showTab(mTabIndex);
    updateUi();
}

AppSettingsDialog::~AppSettingsDialog()
{
    delete mSettingsData;
    delete ui;
}

const AppSettingsData * AppSettingsDialog::data() const
{
    return mSettingsData;
}

int AppSettingsDialog::currentTab() const
{
    return ui->tabWidget->currentIndex();
}

void AppSettingsDialog::initialize(const AppSettingsData * data)
{
    ui->grpDebugger->setChecked(data->debugger.use);
    ui->edtDebuggerCommand->setText(data->debugger.command);
    ui->edtDebuggerParams->setText(data->debugger.parameters);
    ui->grpTelnet->setChecked(data->debugger.telnet.use);

    ui->edtTerminalPort->setText(data->terminal.port);
    ui->cbxClearInputAfterSent->setChecked(mSettingsData->terminal.clearInputAfterSent);

    // fill combos
    fillComboBox(ui->cbxTerminalBaudrate, AppManager::terminalBaudrates(), mSettingsData->terminal.baudrate);
    fillComboBox(ui->cbxTerminalDataBits, AppManager::terminalDataBits(),  mSettingsData->terminal.databits);
    fillComboBox(ui->cbxTerminalParity,   AppManager::terminalParity(),    mSettingsData->terminal.parity);
    fillComboBox(ui->cbxTerminalStopBits, AppManager::terminalStopBits(),  mSettingsData->terminal.stopbits);
}

void AppSettingsDialog::showTab(int tabIdx)
{
    for (int i = 0; i < ui->tabWidget->count(); ++i)
        ui->tabWidget->setTabEnabled(i, i == tabIdx);
}

void AppSettingsDialog::updateUi()
{
    bool modified = false;
    switch (ui->tabWidget->currentIndex()) {
//    case TabCommon:
//        break;
    case TabDebugger:
        modified |= isModified(mSettingsData->debugger.use, ui->grpDebugger->isChecked());
        modified |= isModified(mSettingsData->debugger.command, ui->edtDebuggerCommand->text());
        modified |= isModified(mSettingsData->debugger.parameters, ui->edtDebuggerParams->text());
        modified |= isModified(mSettingsData->debugger.telnet.use, ui->grpTelnet->isChecked());
        break;
    case TabTerminal:
        modified |= isModified(mSettingsData->terminal.port, ui->edtTerminalPort->text());
        modified |= isModified(mSettingsData->terminal.clearInputAfterSent,
                               ui->cbxClearInputAfterSent->isChecked());
        modified |= isModified(mSettingsData->terminal.baudrate, ui->cbxTerminalBaudrate->currentData().toInt());
        modified |= isModified(mSettingsData->terminal.databits, ui->cbxTerminalDataBits->currentData().toInt());
        modified |= isModified(mSettingsData->terminal.parity, ui->cbxTerminalParity->currentData().toInt());
        modified |= isModified(mSettingsData->terminal.stopbits, ui->cbxTerminalStopBits->currentData().toInt());
        break;
    }

    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(modified);
}

void AppSettingsDialog::applySettings()
{
    mSettingsData->debugger.use = ui->grpDebugger->isChecked();
    mSettingsData->debugger.command = ui->edtDebuggerCommand->text();
    mSettingsData->debugger.parameters = ui->edtDebuggerParams->text();
    mSettingsData->debugger.telnet.use = ui->grpTelnet->isChecked();
    mSettingsData->terminal.port = ui->edtTerminalPort->text();
    mSettingsData->terminal.clearInputAfterSent = ui->cbxClearInputAfterSent->isChecked();
    mSettingsData->terminal.baudrate = ui->cbxTerminalBaudrate->currentData().toInt();
    mSettingsData->terminal.databits = ui->cbxTerminalDataBits->currentData().toInt();
    mSettingsData->terminal.parity = ui->cbxTerminalParity->currentData().toInt();
    mSettingsData->terminal.stopbits = ui->cbxTerminalStopBits->currentData().toInt();
}

void AppSettingsDialog::changed(QString)
{
    updateUi();
}

void AppSettingsDialog::changed(int)
{
    updateUi();
}

void AppSettingsDialog::changed(bool)
{
    updateUi();
}

