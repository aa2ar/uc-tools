/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   appsettingsdialog.h
* \author Alex Arefyev
* \date   08.02.2018
*
*
*/

#ifndef __APPSETTINGS_DIALOG__INCLUDED__
#define __APPSETTINGS_DIALOG__INCLUDED__

#include <QDialog>

namespace Ui {
class SettingsDialog;
}

class AppSettings;
class AppSettingsData;
class AppSettingsDialog : public QDialog
{
    Q_OBJECT

public:
    AppSettingsDialog(const AppSettings * as, int tabIdx, QWidget * parent = 0);
    ~AppSettingsDialog();

    const AppSettingsData * data() const;
    int currentTab() const;

private slots:
    void applySettings();
    void changed(QString);
    void changed(int);
    void changed(bool);

private:
    enum {
        TabCommon = -1,
        TabDebugger = 0,
        TabTerminal = 1,
    };

    Ui::SettingsDialog *ui;
    AppSettingsData * mSettingsData = nullptr;

    void initialize(const AppSettingsData *);
    void showTab(int);
    void updateUi();
};

#endif // __APPSETTINGS_DIALOG__INCLUDED__
