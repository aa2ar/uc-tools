/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   collapsiblewidget.cpp
* \author Alex Arefyev
* \date   08.02.2018
*
*
*/

#include "collapsiblewidget.h"
#include <QPaintEvent>
#include <QStyleOptionFrame>
#include <QPainter>
#include <QToolTip>

class CollapsibleWidgetManager::Private
{
    friend class CollapsibleWidgetManager;

    QList<CollapsibleWidget *> widgets;

    bool contains(CollapsibleWidget * w);
    CollapsibleWidget * findNextCollapsible(CollapsibleWidget *);
    bool allCollapsed();
};

bool CollapsibleWidgetManager::Private::contains(CollapsibleWidget * w)
{
    return widgets.indexOf(w) > -1;
}

CollapsibleWidget * CollapsibleWidgetManager::Private::findNextCollapsible(CollapsibleWidget * w)
{
    if (widgets.size() == 1) return nullptr;

    int idx = widgets.indexOf(w);
    if (idx > -1) {
        int new_idx = idx;
        do {
            new_idx++;
            if (new_idx >= widgets.size())
                new_idx = 0;

            if (new_idx == idx) break;

            CollapsibleWidget * ww = widgets[new_idx];
            if (ww->isVisible())// && ww->isCollapsed())
                return ww;
        }
        while (1);
    }
    return nullptr;
}

bool CollapsibleWidgetManager::Private::allCollapsed()
{
    foreach (CollapsibleWidget * w, widgets)
        if (!w->isCollapsed() && w->isVisible())
            return false;

    return true;
}

// CollapsibleWidgetManager
CollapsibleWidgetManager::CollapsibleWidgetManager()
    : p(new CollapsibleWidgetManager::Private)
{}

CollapsibleWidgetManager::~CollapsibleWidgetManager()
{
    delete p;
}

void CollapsibleWidgetManager::addCWidget(CollapsibleWidget * w)
{
    if (!p->contains(w))
        p->widgets.append(w);
}

void CollapsibleWidgetManager::removeCWidget(CollapsibleWidget * w)
{
    p->widgets.removeOne(w);
}

bool CollapsibleWidgetManager::canCollapse(CollapsibleWidget * w)
{
    return p->findNextCollapsible(w) != nullptr;
}

void CollapsibleWidgetManager::collapsed(CollapsibleWidget * w)
{
    if (p->allCollapsed()) {
        CollapsibleWidget * ww = p->findNextCollapsible(w);
        if (ww)
            ww->expand();
    }
}

void CollapsibleWidgetManager::hid(CollapsibleWidget * w)
{
    if (p->allCollapsed()) {
        CollapsibleWidget * ww = p->findNextCollapsible(w);
        if (ww)
            ww->expand();
    }
}



// CollapsibleWidget::Private
class CollapsibleWidget::Private
{
    friend class CollapsibleWidget;

    Private(CollapsibleWidget * w) : q(w) {}

    CollapsibleWidget * q;
    CollapsibleWidgetManager * manager = nullptr;
    QSize iconSize = {16, 16};
    int   headerSize = 20;
    QFont headerFont;
    QIcon icons[2];
    QWidget * widget = nullptr;
    enum State {
        Collapsed = 0,
        Expanded  = 1
    } state = Expanded;
    enum {
        IconOffset = 2,
        InfinitHeight = 16777215,
    };

    int calcHeaderHeight();
    QRect headerRect();
    void updateLayout();
};

QRect CollapsibleWidget::Private::headerRect()
{
    return {0, 0, q->width(), headerSize};
}

int CollapsibleWidget::Private::calcHeaderHeight()
{
    int h = qMax(iconSize.height(), headerFont.pixelSize());
    return h + IconOffset + IconOffset;
}

void CollapsibleWidget::Private::updateLayout()
{
    if (widget) {
        int d = q->frameWidth();
        //if (d < 1) d = 1;
        widget->setGeometry(d, headerSize, q->width()-d-d, q->height() - headerSize-d);
    }
}


// CollapsibleWidget
CollapsibleWidget::CollapsibleWidget(QWidget *parent)
    : QFrame(parent)
    , p(new Private(this))
{
    CollapsibleWidgetManager * mgr = dynamic_cast<CollapsibleWidgetManager *>(parent);
    if (mgr){
        p->manager = mgr;
        p->manager->addCWidget(this);
    }

    p->headerFont = font();
    p->headerFont.setBold(true);
}

CollapsibleWidget::~CollapsibleWidget()
{
    delete p;
}

void CollapsibleWidget::setManager(CollapsibleWidgetManager * m)
{
    p->manager = m;
}

void CollapsibleWidget::collapse()
{
    p->state = Private::Collapsed;

    if (p->widget)
        p->widget->setVisible(false);

    setMinimumHeight(p->headerSize);
    setMaximumHeight(p->headerSize);

    if (p->manager) p->manager->collapsed(this);
    emit collapsed();
}

void CollapsibleWidget::expand()
{
    p->state = Private::Expanded;

    int minHeight = sizeHint().height();

    if (p->widget) {
        int wMinHeight = p->widget->minimumHeight();
        if (wMinHeight > 0)
            minHeight = p->headerSize + wMinHeight;
        else
            minHeight = p->headerSize + p->widget->sizeHint().height();

        p->widget->setVisible(true);
    }

    setMinimumHeight(minHeight);
    setMaximumHeight(Private::InfinitHeight);
}

bool CollapsibleWidget::isCollapsed() const
{
    return p->state == Private::Collapsed;
}

void CollapsibleWidget::setIconSize(QSize sz)
{
    p->iconSize = sz;
    p->headerSize = p->calcHeaderHeight();
    p->updateLayout();
}

void CollapsibleWidget::setIcons(QIcon i1, QIcon i2)
{
    p->icons[0] = i1;
    p->icons[1] = i2;
}

void CollapsibleWidget::setWidget(QWidget * w)
{
    p->widget = w;
    p->widget->setParent(this);

    if (p->state == Private::Expanded) {
        int minHeight = sizeHint().height();
        int wMinHeight = p->widget->minimumHeight();
        if (wMinHeight > 0)
            minHeight = p->headerSize + wMinHeight;
        else
            minHeight = p->headerSize + p->widget->sizeHint().height();

        setMinimumHeight(minHeight);
        setMaximumHeight(Private::InfinitHeight);
    }
    else {
        setMinimumHeight(p->headerSize);
        setMaximumHeight(p->headerSize);
    }

    p->updateLayout();
    p->widget->setVisible(p->state == Private::Expanded);
}

bool CollapsibleWidget::isWidgetEnabled() const
{
    return p->widget ? p->widget->isEnabled() : false;
}

void CollapsibleWidget::setWidgetEnabled(bool enabled)
{
    if (p->widget) p->widget->setEnabled(enabled);
}

bool CollapsibleWidget::event(QEvent * event)
{
    if (event->type() == QEvent::ToolTip) {
        QHelpEvent * helpEvent = static_cast<QHelpEvent *>(event);
        if (helpEvent->pos().y() < p->headerSize)
            QToolTip::showText(helpEvent->globalPos(), toolTip());
        else
            helpEvent->ignore();

        return true;
    }
    return QWidget::event(event);
}

void CollapsibleWidget::drawFrame(QPainter * painter)
{
    QRect headerRect = p->headerRect();
    int lh = p->headerSize; //qMax(mHeaderSize, height() / 2);

    bool enabled = isEnabled();

    QPen oldPen = painter->pen();

    QColor bg = palette().background().color();
    QColor fg = enabled ? palette().dark().color() : palette().mid().color();

    QLinearGradient gr;
    gr.setColorAt(0, fg);
    gr.setColorAt(1.0, bg);
    gr.setStart(headerRect.topLeft());

    // horz line
    gr.setFinalStop(headerRect.left() + headerRect.width(), headerRect.top() + 1);
    painter->setPen(QPen(QBrush(gr), 1));
    painter->drawLine(headerRect.left(), headerRect.top(),
                headerRect.left() + headerRect.width(),
                headerRect.top() + 1);

    // vert line
    gr.setFinalStop(headerRect.left() + 1, headerRect.top() + lh);
    painter->setPen(QPen(QBrush(gr), 1));
    painter->drawLine(headerRect.left(), headerRect.top(),
                headerRect.left() + 1,
                headerRect.top() + lh);

    painter->setPen(oldPen);
}

void CollapsibleWidget::drawHeader(QPainter * painter)
{
    QRect headerRect = p->headerRect();

    bool enabled = isEnabled();

    int min = qMin(p->iconSize.width(), p->iconSize.height());
    QRect iconRect(0, 0, min, min);
    iconRect.moveTo(Private::IconOffset + (p->iconSize.width() - min) / 2,
                    Private::IconOffset + (p->iconSize.height() - min) / 2);

    QIcon::Mode mode = enabled ? QIcon::Normal : QIcon::Disabled;

    painter->drawPixmap(iconRect, p->icons[p->state].pixmap(p->iconSize*2, mode));
        headerRect.setLeft(Private::IconOffset + p->iconSize.width() + Private::IconOffset);
        painter->setFont(p->headerFont);
        painter->drawText(headerRect, Qt::AlignLeft|Qt::AlignVCenter, windowTitle());
}

void CollapsibleWidget::paintEvent(QPaintEvent * event)
{
    if (frameShape() == NoFrame) {
        QPainter paint(this);
        drawFrame(&paint);
        drawHeader(&paint);
    }
    else {
        QFrame::paintEvent(event);
        QPainter paint(this);
        drawHeader(&paint);
    }
}

void CollapsibleWidget::resizeEvent(QResizeEvent *)
{
    p->updateLayout();
}

void CollapsibleWidget::mousePressEvent(QMouseEvent * event)
{
    if (event->y() > p->headerSize)
        return;

    if (p->manager) {
        if (p->state == Private::Collapsed)
            expand();
        else if (p->manager->canCollapse(this))
            collapse();
    }
    else {
        if (p->state == Private::Collapsed)
            expand();
        else
            collapse();
    }
}

void CollapsibleWidget::hideEvent(QHideEvent *)
{
    if (p->manager) p->manager->hid(this);
    emit hid();
}

void CollapsibleWidget::changeEvent(QEvent * event)
{
    if (event->type() == QEvent::ParentChange) {
        if (p->manager) p->manager->removeCWidget(this);
        p->manager = dynamic_cast<CollapsibleWidgetManager *>(parent());
        if (p->manager) p->manager->addCWidget(this);
    }
}
