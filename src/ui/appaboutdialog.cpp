/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   appaboutdialog.cpp
* \author Alex Arefyev
* \date   08.02.2018
*
*
*/

#include "appaboutdialog.h"
#include "ui_appaboutdialog.h"

AppAboutDialog::AppAboutDialog(QWidget * parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog);
}
