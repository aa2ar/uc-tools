/**
* \file   debugger.h
* \author Alex Arefyev
* \date   06.02.2018
* 
* 
*/

#ifndef __DEBUGGER__INCLUDED__
#define __DEBUGGER__INCLUDED__

class Debugger
{
public:
	Debugger();
	~Debugger();

protected:
private:
};

#endif /* __DEBUGGER__INCLUDED__ */
