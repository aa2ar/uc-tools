/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   appconfig.cpp
* \author Alex Arefyev
* \date   08.02.2018
*
*
*/

#include "appconfig.h"
#include <QSettings>
#include <QStringList>
#include <QScreen>
#include <QApplication>
#include <QDebug>

QRect parseGeometry(const QString & geom)
{
    QRect result;

    QStringList parts = geom.split('-');
    if (parts.size() == 2) {
        QStringList pos = parts[0].split(',');
        if (pos.size() == 2) {
            bool ok = false;
            int x = pos[0].toInt(&ok);
            if (ok) {
                int y = pos[1].toInt(&ok);
                if (ok) {
                    QStringList size = parts[1].split('x');
                    if (size.size() == 2) {
                        bool ok = false;
                        int w = size[0].toInt(&ok);
                        if (ok) {
                            int h = size[1].toInt(&ok);
                            if (ok)
                                result = {x, y, w, h};
                        }
                    }
                }
            }
        }
    }

    return result;
}

bool AppConfig::load(const QString & filePath)
{
    mFilePath = filePath;

    QSettings ss(mFilePath, QSettings::IniFormat);

    ss.beginGroup(QStringLiteral("app"));
    QVariant var = ss.value(QStringLiteral("geom"));
    if (var.isValid()) {
        mGeometry = parseGeometry(var.toString());

        QRect screenRect = QApplication::primaryScreen()->availableGeometry();
        if (mGeometry.width() > screenRect.width())
            mGeometry.setWidth(screenRect.width());
        if (mGeometry.height() > screenRect.height())
            mGeometry.setHeight(screenRect.height());
        if (mGeometry.left() < screenRect.left())
            mGeometry.moveLeft(screenRect.left());
        else
        if (mGeometry.left() >= screenRect.right())
            mGeometry.moveLeft(screenRect.right() - screenRect.width() / 10);
        if (mGeometry.top() < screenRect.top())
            mGeometry.moveTop(screenRect.top());
        else
        if (mGeometry.top() >= screenRect.bottom())
            mGeometry.moveTop(screenRect.bottom() - screenRect.height() / 10);

    }

    ss.endGroup();

    return true;
}

bool AppConfig::save() const
{
    QSettings ss(mFilePath, QSettings::IniFormat);

    ss.beginGroup(QStringLiteral("app"));
    ss.setValue(QStringLiteral("geom"), QString("%1,%2-%3x%4")
                .arg(mGeometry.x()).arg(mGeometry.y())
                .arg(mGeometry.width()).arg(mGeometry.height()));

    ss.endGroup();

    return true;
}

QRect AppConfig::geometry() const
{
    return mGeometry;
}

void AppConfig::setGeometry(const QRect & geom)
{
    mGeometry = geom;
}
