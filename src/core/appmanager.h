/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   appmanager.h
* \author Alex Arefyev
* \date   08.02.2018
*
*
*/

#ifndef __APPMANAGER__INCLUDED__
#define __APPMANAGER__INCLUDED__

#include <QObject>
#include <QStack>
#include <QSet>
#include <QProcess>
#include <QSerialPort>
#include "protocol/protocol.h"

class QProcess;
class QSerialPort;
class Protocol;
class AppSettings;
class AppSettingsData;
class AppManager : public QObject, public ProtocolCallback
{
    Q_OBJECT

public:
    explicit AppManager(AppSettings * as, QObject *parent = 0);
    ~AppManager();

    const AppSettings * settings() const;
    void setSettings(unsigned, const AppSettingsData *);

    unsigned state() const;

    static const QList<QPair<QString, int> > & terminalBaudrates();
    static const QList<QPair<QString, int> > & terminalDataBits();
    static const QList<QPair<QString, int> > & terminalParity();
    static const QList<QPair<QString, int> > & terminalStopBits();

    static const QStringList & telnetCommands();

    bool start();
    void restartDebugger();
    void restartTelnet();
    void restartTerminal();

    // ProtocolCallback
    void dataReady(const QVector<QByteArray> &) override;

signals:
    void logMessage(QString);
    void logError(QString);
    void event(unsigned);
    void debuggerOutput(QString);
    void telnetOutput(QString);
    void terminalOutput(const QVector<QByteArray> &);

public slots:
    void telnetInput(QString);
    void terminalInput(QString);

private slots:
    void onDebuggerStderr();
    void onDebuggerStdoutput();
    void onDebuggerFinished(int, QProcess::ExitStatus);
    void onTelnetStderr();
    void onTelnetStdoutput();
    void onTelnetFinished(int, QProcess::ExitStatus);
    void readSerialPort();

private:
    unsigned mAppState = 0;
    AppSettings * mSettings = nullptr;
    QProcess * mDebuggerProcess = nullptr;
    QProcess * mTelnetProcess = nullptr;
    QSerialPort * mSerialPort = nullptr;
    Protocol * mProtocol = nullptr;

    enum LogType {
        LogMessage,
        LogError
    };
    void log(LogType, const char * _str, ...);
    void startDebugger(const QString & cmd, const QString & params);
    void stopDebugger();
    void startTelnet(const QString & cmd, const QString & params);
    void stopTelnet();
    void startSerialPort(const QString & port, int baudrate,
                         QSerialPort::DataBits databits,
                         QSerialPort::Parity parity,
                         QSerialPort::StopBits stopbits);
    void stopSerialPort();
};

#endif // __APPMANAGER__INCLUDED__
