/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   appsettings.h
* \author Alex Arefyev
* \date   08.02.2018
*
*
*/

#ifndef __APPSETTINGS__INCLUDED__
#define __APPSETTINGS__INCLUDED__

#include <QObject>
#include <QStack>
#include <QSet>
#include <QProcess>

class AppSettingsData;
class AppSettings : public QObject
{
    Q_OBJECT

public:
    explicit AppSettings(QObject *parent = 0);
    ~AppSettings();

    bool load(const QString &);
    bool save();

    const AppSettingsData * data() const;
    void copySettings(AppSettingsData *) const;
    unsigned setSettings(unsigned, const AppSettingsData *);

private:
    AppSettingsData * mData = nullptr;
    QString mFilePath;
};

#endif // __APPSETTINGS__INCLUDED__
