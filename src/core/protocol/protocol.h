/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   protocol.h
* \author Alex Arefyev
* \date   08.02.2018
* 
* 
*/

#ifndef __PROTOCOL__INCLUDED__
#define __PROTOCOL__INCLUDED__

#include <QByteArray>

struct ProtocolItem
{
    ProtocolItem();
    ProtocolItem(const QByteArray *);
    const QByteArray * data;
};

struct ProtocolCallback
{
    virtual ~ProtocolCallback() = default;
    virtual void dataReady(const QVector<QByteArray> &) = 0;
};

class Protocol
{
public:
    Protocol(ProtocolCallback *);
    virtual ~Protocol() = default;

    void addData(const QByteArray &);


protected:
    ProtocolCallback * mCallback = nullptr;
    QByteArray mData;
    char mTerminator = 0;

    virtual void processPdu(const QByteArray &) = 0;

private:
};

#endif /* __PROTOCOL__INCLUDED__ */
