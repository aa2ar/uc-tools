/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   protocol.cpp
* \author Alex Arefyev
* \date   08.02.2018
* 
* 
*/

#include "protocol.h"

Protocol::Protocol(ProtocolCallback * cb)
    : mCallback(cb)
{
    Q_ASSERT(cb);

    mData.reserve(256);
}

void Protocol::addData(const QByteArray & data)
{
    if (mTerminator == 0)
        processPdu(data);
    if (mData.size() + data.size() <= 256) {
        int idx = 0;
        for (;;) {
            int idx2 = data.indexOf(mTerminator, idx);
            if (idx2 > -1) {
                mData.append(data.mid(idx, idx2-idx));

                processPdu(mData);

                mData.clear();
                idx = idx2+1;
            }
            else {
                mData.append(data.mid(idx));
                break;
            }
        }
    }
}




ProtocolItem::ProtocolItem()
    : data(0)
{}

ProtocolItem::ProtocolItem(const QByteArray * data)
    : data(data)
{}
