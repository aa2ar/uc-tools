/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   text.h
* \author Alex Arefyev
* \date   08.02.2018
* 
* 
*/

#ifndef __PROTOCOL_TEXT__INCLUDED__
#define __PROTOCOL_TEXT__INCLUDED__

#include "protocol.h"

class TextProtocol : public Protocol
{
public:
    TextProtocol(ProtocolCallback *);

protected:
    void processPdu(const QByteArray &) override;
};

#endif /* __PROTOCOL_TEXT__INCLUDED__ */
