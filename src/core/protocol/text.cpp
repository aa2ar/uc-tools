/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   text.cpp
* \author Alex Arefyev
* \date   08.02.2018
* 
* 
*/

#include "text.h"
#include <QDebug>

TextProtocol::TextProtocol(ProtocolCallback * cb)
    : Protocol(cb)
{}

void TextProtocol::processPdu(const QByteArray & pdu)
{
    static QVector<QByteArray> items(1);
    items[0] = pdu;
    mCallback->dataReady(items);
}
