/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   appsettings.h
* \author Alex Arefyev
* \date   08.02.2018
*
*
*/

#include "appsettings.h"
#include "appsettingsdata.h"
#include <QSettings>
#include <QDebug>

AppSettingsData * createAppSettingsData()
{
    AppSettingsData * data = new AppSettingsData;

    // debugger defaults
    data->debugger.use = true;
    data->debugger.command = QStringLiteral("openocd");
    data->debugger.parameters = QStringLiteral("-f interface/stlink.cfg -f target/stm32f1x.cfg");
    data->debugger.telnet.use = true;
    data->debugger.telnet.command = QStringLiteral("telnet");
    data->debugger.telnet.parameters = QStringLiteral("localhost 4444");

    // terminal defaults
    data->terminal.useTerminal = true;
    data->terminal.port = QStringLiteral("/dev/ttyUSB0");
    data->terminal.baudrate = 115200;
    data->terminal.databits = 8;
    data->terminal.parity = 0;
    data->terminal.stopbits = 1;
    data->terminal.clearInputAfterSent = true;

    return data;
}

AppSettings::AppSettings(QObject * parent)
    : QObject(parent)
    , mData(createAppSettingsData())
{

}

AppSettings::~AppSettings()
{
    delete mData;
}

template <typename T>
void setValue(QSettings & ss, const QString & key, T & target)
{
    if (ss.contains(key))
        target = ss.value(key).value<T>();
}

QString packPortSettings(int br, int db, int prt, int sb)
{
    const char * parity = prt == 0 ? "N" : (prt == 1 ? "E" : "O");
    return QString("%1,%2%3%4").arg(br).arg(db).arg(parity).arg(sb);
}

void unpackPortSettings(const QString & value, int & br, int & db, int & prt, int & sb)
{
    QStringList parts = value.split(',');
    bool ok = false;
    int _br = parts[0].toInt(&ok); if (ok) br = _br;
    if (parts.size() > 1) {
        QString s = parts[1];
        int parity = 0;
        int i = s.indexOf('N');
        if (i > 0) parity = 0;
        else {
            i = s.indexOf('E');
            if (i > 0) parity = 1;
            else {
                i = s.indexOf('O');
                if (i > 0) parity = 2;
            }
        }
        if (i > 0) {
            prt = parity;
            int _db = s.left(i).toInt(&ok);
            if (ok) db = _db;
            int _sb = s.mid(i+1).toInt(&ok);
            if (ok) sb = _sb;
        }
    }
}

bool AppSettings::load(const QString & path)
{
    mFilePath = path;

    QSettings ss(mFilePath, QSettings::IniFormat);

    ss.beginGroup(QStringLiteral("debugger"));
    setValue(ss, QStringLiteral("use_debugger"), mData->debugger.use);
    setValue(ss, QStringLiteral("command"), mData->debugger.command);
    setValue(ss, QStringLiteral("parameters"), mData->debugger.parameters);
    setValue(ss, QStringLiteral("use_telnet"), mData->debugger.telnet.use);
    ss.endGroup();

    ss.beginGroup(QStringLiteral("terminal"));
    setValue(ss, QStringLiteral("use_terminal"), mData->terminal.useTerminal);
    setValue(ss, QStringLiteral("port"), mData->terminal.port);
    QString portSettings;
    setValue(ss, QStringLiteral("port_settings"), portSettings);
    unpackPortSettings(portSettings,
                       mData->terminal.baudrate,
                       mData->terminal.databits,
                       mData->terminal.parity,
                       mData->terminal.stopbits);
    setValue(ss, QStringLiteral("clear_input_after_sent"), mData->terminal.clearInputAfterSent);
    ss.endGroup();

    return true;
}

bool AppSettings::save()
{
    QSettings ss(mFilePath, QSettings::IniFormat);
    ss.beginGroup(QStringLiteral("debugger"));
    ss.setValue(QStringLiteral("use_debugger"), mData->debugger.use);
    ss.setValue(QStringLiteral("command"), mData->debugger.command);
    ss.setValue(QStringLiteral("parameters"), mData->debugger.parameters);
    ss.setValue(QStringLiteral("use_telnet"), mData->debugger.telnet.use);
    ss.endGroup();

    ss.beginGroup(QStringLiteral("terminal"));
    ss.setValue(QStringLiteral("use_terminal"), mData->terminal.useTerminal);
    ss.setValue(QStringLiteral("port"), mData->terminal.port);
    QString portSettings = packPortSettings(mData->terminal.baudrate,
                                        mData->terminal.databits,
                                        mData->terminal.parity,
                                        mData->terminal.stopbits);
    qDebug() << portSettings;
    ss.setValue(QStringLiteral("port_settings"), portSettings);
    ss.setValue(QStringLiteral("clear_input_after_sent"), mData->terminal.clearInputAfterSent);
    ss.endGroup();

    return true;
}

const AppSettingsData * AppSettings::data() const
{
    return mData;
}

void AppSettings::copySettings(AppSettingsData * asd) const
{
    if (asd)
        *asd = *mData;
}

unsigned AppSettings::setSettings(unsigned cat, const AppSettingsData * asd)
{
    if (asd == nullptr) return 0;

    unsigned result = 0;
    if (cat & AppSettingsData::Debugger) {
        if (asd->debugger.use != mData->debugger.use) result |= RestartDebugger;
        if (asd->debugger.command != mData->debugger.command) result |= RestartDebugger;
        if (asd->debugger.parameters != mData->debugger.parameters) result |= RestartDebugger;
        if (asd->debugger.telnet.use != mData->debugger.telnet.use) result |= RestartTelnet;
    }
    if (cat & AppSettingsData::Terminal) {
        if (asd->terminal.useTerminal != mData->terminal.useTerminal) result |= RestartTerminal;
        if (asd->terminal.port != mData->terminal.port) result |= RestartTerminal;
        if (asd->terminal.baudrate != mData->terminal.baudrate) result |= RestartTerminal;
        if (asd->terminal.databits != mData->terminal.databits) result |= RestartTerminal;
        if (asd->terminal.parity != mData->terminal.parity) result |= RestartTerminal;
        if (asd->terminal.stopbits != mData->terminal.stopbits) result |= RestartTerminal;
    }

    *mData = *asd;

    save();

    return result;
}
