/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   terminalhistory.cpp
* \author Alex Arefyev
* \date   06.02.2018
* 
* 
*/

#include "terminalhistory.h"
#include <QFile>
#include <QTextStream>

TerminalHistory::TerminalHistory()
{
}

TerminalHistory::~TerminalHistory()
{
}

bool TerminalHistory::load(const QString & filePath)
{
    mFilePath = filePath;

    QFile textFile(mFilePath);
    if (!textFile.open(QIODevice::ReadOnly | QFile::Text))
        return false;

    QTextStream textStream(&textFile);
    while (true)
    {
        QString line = textStream.readLine();
        if (line.isNull())
            break;
        else
            mData.append(line);
    }
    return true;
}

bool TerminalHistory::save() const
{
    QFile textFile(mFilePath);
    if (!textFile.open(QIODevice::WriteOnly | QFile::Truncate | QFile::Text))
        return false;

    QTextStream textStream(&textFile);

    foreach (QString s, mData)
        textStream << s << QStringLiteral("\n");

    return true;
}

const QStringList & TerminalHistory::get() const
{
    return mData;
}

void TerminalHistory::set(const QStringList & data)
{
    mData = data;
    save();
}

