#ifndef APPSETTINGSDATA_H
#define APPSETTINGSDATA_H

#include <QString>

enum {
    RestartDebugger = 0x0001,
    RestartTelnet   = 0x0002,
    RestartTerminal = 0x0004,

    StateChanged    = 0x8000,
};


struct AppSettingsData
{
    enum {
        Debugger = 0x0001,
        Terminal = 0x0002
    };

    struct {
        bool    use;
        QString command;
        QString parameters;
        struct {
            bool    use;
            QString command;
            QString parameters;
        } telnet;
    } debugger;

    struct {
        bool    useTerminal;
        QString port;
        int     baudrate;
        int     databits;
        int     parity;
        int     stopbits;
        int     protocol; // 0 - no protocol
        bool    clearInputAfterSent;
    } terminal;
};

#endif // APPSETTINGSDATA_H
