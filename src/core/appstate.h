/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   appstate.h
* \author Alex Arefyev
* \date   08.02.2018
* 
* 
*/

#ifndef __APPSTATE__INCLUDED__
#define __APPSTATE__INCLUDED__

struct AppStateData;
class AppState
{
public:
	AppState();
	~AppState();

protected:
private:
};

#endif /* __APPSTATE__INCLUDED__ */
