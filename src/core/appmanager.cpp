/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   appmanager.cpp
* \author Alex Arefyev
* \date   08.02.2018
*
*
*/

#include "appmanager.h"
#include "appsettings.h"
#include "appsettingsdata.h"
#include "appstatedata.h"
#include "protocol/text.h"
#include <QApplication>
#include <QFileSystemWatcher>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QFont>
#include <QTimer>
#include <QTime>
#include <QThread>
#include <QDebug>

static QList<QPair<QString, int>> terminalBaudrateList = {
    { QStringLiteral("1200"),   1200 },
    { QStringLiteral("2400"),   2400 },
    { QStringLiteral("4800"),   4800 },
    { QStringLiteral("9600"),   9600 },
    { QStringLiteral("19200"),  19200 },
    { QStringLiteral("38400"),  38400 },
    { QStringLiteral("57600"),  57600 },
    { QStringLiteral("115200"), 115200 }
};

static QList<QPair<QString, int>> terminalDataBitList = {
    { QStringLiteral("5"), 5 },
    { QStringLiteral("6"), 6 },
    { QStringLiteral("7"), 7 },
    { QStringLiteral("8"), 8 },
};

static QList<QPair<QString, int>> terminalParityList = {
    { QStringLiteral("No parity"), QSerialPort::NoParity },
    { QStringLiteral("Even"),      QSerialPort::EvenParity },
    { QStringLiteral("Odd"),       QSerialPort::OddParity },
    { QStringLiteral("Space"),     QSerialPort::SpaceParity },
    { QStringLiteral("Mark"),      QSerialPort::MarkParity },
};

static QList<QPair<QString, int>> terminalStopBitList = {
    { QStringLiteral("One"),            QSerialPort::OneStop },
    { QStringLiteral("One and a half"), QSerialPort::OneAndHalfStop },
    { QStringLiteral("Two"),            QSerialPort::TwoStop },
};

static QStringList telnetCommandList = {
    QStringLiteral("reset halt"),
    QStringLiteral("reset run"),
};

AppManager::AppManager(AppSettings * as, QObject *parent)
    : QObject(parent)
    , mSettings(as)
{
    mProtocol = new TextProtocol(this);
}

AppManager::~AppManager()
{
    stopTelnet();
    stopDebugger();
    stopSerialPort();
}

const AppSettings * AppManager::settings() const
{
    return mSettings;
}

void AppManager::setSettings(unsigned cat, const AppSettingsData * asd)
{
    unsigned events = mSettings->setSettings(cat, asd);
    if (events) {

        emit event(events);

        if (events & RestartTelnet)
            restartTelnet();
        else if (events & RestartDebugger)
            restartDebugger();

        if (events & RestartTerminal)
            restartTerminal();
    }

    emit event(StateChanged);
}

unsigned AppManager::state() const
{
    return mAppState;
}

const QList<QPair<QString, int> > &AppManager::terminalBaudrates()
{
    return terminalBaudrateList;
}

const QList<QPair<QString, int> > & AppManager::terminalDataBits()
{
    return terminalDataBitList;
}

const QList<QPair<QString, int> > & AppManager::terminalParity()
{
    return terminalParityList;
}

const QList<QPair<QString, int> > & AppManager::terminalStopBits()
{
    return terminalStopBitList;
}

const QStringList & AppManager::telnetCommands()
{
    return telnetCommandList;
}

bool AppManager::start()
{
    emit event(0); // TODO: add 'reset' event???
    restartDebugger();
    restartTerminal();
    return true;
}

void AppManager::restartDebugger()
{
    stopTelnet();
    stopDebugger();

    emit event(RestartDebugger);

    const AppSettingsData * data = mSettings->data();

    if (data->debugger.use) {

        startDebugger(data->debugger.command,
                      data->debugger.parameters);

        if (mDebuggerProcess && data->debugger.telnet.use) {
            QThread::msleep(500);
            startTelnet(data->debugger.telnet.command, data->debugger.telnet.parameters);
        }

        emit event(StateChanged);
    }
}

void AppManager::restartTelnet()
{
    stopTelnet();

    emit event(RestartTelnet);

    const AppSettingsData * data = mSettings->data();

    if (mDebuggerProcess && data->debugger.telnet.use) {
        startTelnet(data->debugger.telnet.command, data->debugger.telnet.parameters);
        emit event(StateChanged);
    }
}

void AppManager::restartTerminal()
{
    stopSerialPort();

    emit event(RestartTerminal);

    const AppSettingsData * data = mSettings->data();
    startSerialPort(data->terminal.port,
                    data->terminal.baudrate,
                    (QSerialPort::DataBits)data->terminal.databits,
                    (QSerialPort::Parity)data->terminal.parity,
                    (QSerialPort::StopBits)data->terminal.stopbits);

    emit event(StateChanged);
}

// ProtocolCallback
void AppManager::dataReady(const QVector<QByteArray> & items)
{
    emit terminalOutput(items);
}

void AppManager::AppManager::startDebugger(const QString & cmd, const QString & params)
{
    if (mDebuggerProcess)
        return; // TODO: should process this error

    mDebuggerProcess = new QProcess(this);

    connect(mDebuggerProcess, SIGNAL(readyReadStandardError()), this, SLOT(onDebuggerStderr()));
    connect(mDebuggerProcess, SIGNAL(readyReadStandardOutput()), this, SLOT(onDebuggerStdoutput()));
    connect(mDebuggerProcess, SIGNAL(finished(int,QProcess::ExitStatus)),
                     this,    SLOT(onDebuggerFinished(int,QProcess::ExitStatus)));

    QStringList args = params.split(' ');
    mDebuggerProcess->start(cmd, args);

    mDebuggerProcess->waitForStarted();

    if(mDebuggerProcess->state() == QProcess::Running)
        mAppState |= DebuggerOK;
    else {
        mAppState &= ~DebuggerOK;
        log(LogError, "Debugger process failed: %s", qPrintable(mDebuggerProcess->errorString()));
        mDebuggerProcess->terminate();
        mDebuggerProcess->kill();
        delete mDebuggerProcess;
        mDebuggerProcess = nullptr;
    }
}

void AppManager::stopDebugger()
{
    mAppState &= ~DebuggerOK;

    if (mDebuggerProcess) {
        mDebuggerProcess->terminate();
        mDebuggerProcess->waitForFinished();
        delete mDebuggerProcess;
        mDebuggerProcess = nullptr;
    }
}

void AppManager::startTelnet(const QString & cmd, const QString & params)
{
    if(mTelnetProcess)
        return; // TODO: need to process this error

    mTelnetProcess = new QProcess(this);

    QObject::connect(mTelnetProcess, SIGNAL(readyReadStandardError()), SLOT(onTelnetStderr()));
    QObject::connect(mTelnetProcess, SIGNAL(readyReadStandardOutput()), SLOT(onTelnetStdoutput()));
    QObject::connect(mTelnetProcess, SIGNAL(finished(int,QProcess::ExitStatus)),
                     this,    SLOT(onTelnetFinished(int,QProcess::ExitStatus)));

    QStringList args = params.split(' ');
    mTelnetProcess->start(cmd, args);

    mTelnetProcess->waitForStarted();

    if(mTelnetProcess->state() == QProcess::Running)
        mAppState |= TelnetOK;
    else {
        mAppState &= ~TelnetOK;
        log(LogError, "Telnet process failed: %s", qPrintable(mTelnetProcess->errorString()));
        mTelnetProcess->terminate();
        mTelnetProcess->kill();
        delete mTelnetProcess;
        mTelnetProcess = nullptr;
    }
}

void AppManager::stopTelnet()
{
    mAppState &= ~TelnetOK;

    if (mTelnetProcess) {
        mTelnetProcess->terminate();
        mTelnetProcess->waitForFinished();
        delete mTelnetProcess;
        mTelnetProcess = nullptr;
    }
}

void AppManager::startSerialPort(const QString &port,
                                 int baudrate,
                                 QSerialPort::DataBits databits,
                                 QSerialPort::Parity parity,
                                 QSerialPort::StopBits stopbits)
{
    if(mSerialPort)
        return; // TODO: should process this error

    mSerialPort = new QSerialPort(this);

    QObject::connect(mSerialPort, SIGNAL(readyRead()), SLOT(readSerialPort()));

    mSerialPort->setPortName(port);
    mSerialPort->setBaudRate(baudrate);
    mSerialPort->setDataBits(databits);
    mSerialPort->setParity(parity);
    mSerialPort->setStopBits(stopbits);

    if (mSerialPort->open(QSerialPort::ReadWrite))
        mAppState |= TerminalOK;
    else {
        mAppState &= ~TerminalOK;
        log(LogError, "Serial port open failed: %s", qPrintable(mSerialPort->errorString()));
        delete mSerialPort;
        mSerialPort = nullptr;
    }
}

void AppManager::stopSerialPort()
{
    mAppState &= ~TerminalOK;

    if (mSerialPort) {
        mSerialPort->close();
        delete mSerialPort;
        mSerialPort = nullptr;
    }
}

void AppManager::onDebuggerStderr()
{
    emit debuggerOutput(mDebuggerProcess->readAllStandardError());
}

void AppManager::onDebuggerStdoutput()
{
    emit debuggerOutput(mDebuggerProcess->readAllStandardOutput());
}

void AppManager::onDebuggerFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    mAppState &= ~DebuggerOK;
    emit event(StateChanged);

    delete mDebuggerProcess;
    mDebuggerProcess = nullptr;

    switch(exitStatus) {
    case QProcess::NormalExit:
        log(LogError, "Debugger process has finished with code %d", exitCode);
        break;
    case QProcess::CrashExit:
        log(LogError, "Debugger process has crashed with code %d", exitCode);
        break;
    }
}

void AppManager::onTelnetStderr()
{
    emit telnetOutput(mTelnetProcess->readAllStandardError());
}

void AppManager::onTelnetStdoutput()
{
    emit telnetOutput(mTelnetProcess->readAllStandardOutput());
}

void AppManager::onTelnetFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    mAppState &= ~TelnetOK;
    emit event(StateChanged);

    delete mTelnetProcess;
    mTelnetProcess = nullptr;

    switch(exitStatus) {
    case QProcess::NormalExit:
        log(LogError, "Telnet process has finished with code %d", exitCode);
        break;
    case QProcess::CrashExit:
        log(LogError, "Telnet process has crashed with code %d", exitCode);
        break;
    }
}

void AppManager::readSerialPort()
{
    mProtocol->addData(mSerialPort->readAll());
}

void AppManager::telnetInput(QString input)
{
    if (mTelnetProcess) {
        mTelnetProcess->write(qPrintable(input));
        mTelnetProcess->write("\n");
    }
}

void AppManager::terminalInput(QString input)
{
    if (mSerialPort)
        mSerialPort->write(qPrintable(input));
}

void AppManager::log(LogType type, const char * _str, ...)
{
    va_list vl;
    va_start(vl, _str);
    QString str;
    str.vsprintf(_str, vl);
    if (type == LogMessage) emit logMessage(str);
    else                    emit logError(str);
    va_end(vl);
}
