/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   terminalhistory.h
* \author Alex Arefyev
* \date   06.02.2018
* 
* 
*/

#ifndef __TERMINALHISTORY__INCLUDED__
#define __TERMINALHISTORY__INCLUDED__

#include <QStringList>

class TerminalHistory
{
public:
    TerminalHistory();
    ~TerminalHistory();

    bool load(const QString &);
    bool save() const;
    const QStringList & get() const;
    void set(const QStringList &);

protected:
private:
    QString mFilePath;
    QStringList mData;
};

#endif /* __TERMINALHISTORY__INCLUDED__ */
