/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   appconfig.h
* \author Alex Arefyev
* \date   08.02.2018
*
*
*/

#ifndef __APPCONFIG__INCLUDED__
#define __APPCONFIG__INCLUDED__

#include <QString>
#include <QRect>

class AppConfig
{
public:
    AppConfig() = default;

    bool load(const QString &);
    bool save() const;

    QRect geometry() const;
    void setGeometry(const QRect &);

private:
    QString mFilePath;
    QRect mGeometry;
};

#endif // __APPCONFIG__INCLUDED__
