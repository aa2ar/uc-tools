#ifndef APPSTATEDATA_H
#define APPSTATEDATA_H

enum
{
    DebuggerOK  = 0x0001,
    TelnetOK    = 0x0002,
    TerminalOK  = 0x0004,
};

#endif // APPSTATEDATA_H
