/* Copyright © 2018 Alexander Arefyev */
/* License: MIT                       */
/* Contacts: alex.arefyev@gmail.com   */

/**
* \file   main.cpp
* \author Alex Arefyev
* \date   08.02.2018
*
*
*/
#include "ui/mainwindow.h"
#include "core/appmanager.h"
#include "core/appsettings.h"
#include "core/appconfig.h"
#include "core/terminalhistory.h"
#include <QApplication>
#include <QStandardPaths>
#include <QDir>
#include <iostream>

#define COMPANY_NAME "ar72"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName(QStringLiteral("uc-tools"));
    a.setApplicationDisplayName(QStringLiteral("uC Tools"));
    a.setApplicationVersion(QStringLiteral("1.0"));

    QString configDir = QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);
    if (!configDir.isEmpty()) {
        configDir += QDir::separator() + QStringLiteral(COMPANY_NAME) + QDir::separator() +a.applicationName() + QDir::separator();
        QDir().mkpath(configDir);
    }

    AppConfig config;
    config.load(configDir + QStringLiteral("app.conf"));

    AppSettings settings;
    settings.load(configDir + QStringLiteral("settings.conf"));

    AppManager mgr(&settings);
    QObject::connect(&mgr, &AppManager::logMessage, &a, [](QString msg){
        std::cout << qPrintable(msg) << std::endl;
    });
    QObject::connect(&mgr, &AppManager::logError, &a, [](QString err){
        std::cout << qPrintable(err) << std::endl;
    });

    TerminalHistory hist;
    hist.load(configDir + QStringLiteral("terminalhistory"));

    MainWindow w(&mgr, &hist);
    QRect geom = config.geometry();
    if (!geom.isEmpty())
        w.setGeometry(geom);

    w.show();

    int ret = a.exec();

    config.setGeometry(w.geometry());
    config.save();

    return ret;
}
